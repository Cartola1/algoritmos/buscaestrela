from typing import List
from warnings import warn
from heapq import heapify, heappush, heappop
from math import dist, sqrt
import numpy as np
from Nodo.nodo import Node
from Maze_generator.maze_generator import make_maze
from pyfiglet import Figlet


def return_path(current_node) -> list:
    path = []
    current = current_node
    while current is not None:
        path.append(current.position)
        current = current.parent
    return path  # Return reversed path


# @profile
def astar(maze, start, end, shape) -> list:
    """
    Return a list of tuples as a path from the given start to the given end in the given maze.

    :param maze:
    :param start:
    :param end:
    :return:
    """
    # Create start and end node
    start_node = Node(None, start)
    end_node = Node(None, end)

    # Initialize both open and closed list
    open_list: List = []
    closed_list: List = []

    # Heapify the open_list and Add the start node
    heapify(open_list)
    heappush(open_list, start_node)

    # Adding a stop condition
    outer_iterations = 0
    max_iterations = pow((shape[0] * shape[1]), 2)

    # what squares do we search
    adjacent_squares = ((0, -1), (0, 1), (-1, 0), (1, 0))

    # Loop until you find the end
    while open_list:
        outer_iterations += 1

        # Get the current node
        current_node = heappop(open_list)
        heappush(closed_list, current_node)

        if outer_iterations > max_iterations:
            # if we hit this point return the path such as it is
            # it will not contain the destination
            warn("giving up on pathfinding too many iterations")
            return return_path(current_node)

        # Found the goal
        if current_node == end_node:
            print(f"Custo total do caminho: {current_node.f} passos.\n")
            return return_path(current_node)

        # Generate children
        children = []

        for new_position in adjacent_squares:  # Adjacent squares

            # Get node position
            node_position = (
                current_node.position[0] + new_position[0],
                current_node.position[1] + new_position[1],
            )

            # Make sure within range
            if (
                node_position[0] > ((shape[0]) - 1)
                or node_position[0] < 0
                or node_position[1] > ((shape[1]) - 1)
                or node_position[1] < 0
            ):
                continue

            # Make sure walkable terrain
            if maze[node_position[0]][node_position[1]] != 0:
                continue

            # Create new node
            new_node = Node(current_node, node_position)

            # Append
            children.append(new_node)

        # Loop through children
        for child in children:
            # Child is on the closed list
            if child in closed_list:
                continue

            # add cost
            child.g = current_node.g + 1

            # Create the f, g, and h values
            child.h = pow((child.position[0] - end_node.position[0]), 2) + pow(
                (child.position[1] - end_node.position[1]), 2
            )

            child.f = child.g + child.h

            # Child is already in the open list
            if [
                open_node
                for open_node in open_list
                if child.position == open_node.position and child.g > open_node.g
            ]:
                continue

            # Add the child to the open list
            heappush(open_list, child)

    warn("Couldn't get a path to destination")
    return return_path(current_node)


def executeSearch(name):
    maze = np.loadtxt(name, dtype="i", delimiter=",")
    shape = np.shape(maze)
    start: tuple[int] = (1, 1)
    print(type(start))
    end: tuple[int] = (shape[0] - 2, shape[1] - 2)
    path: List[tuple(int)] = astar(maze, start, end, shape)
    for step in path:
        maze[step[0]][step[1]] = 2
    for row in maze:
        line = []
        for col in row:
            if col == 1:
                line.append("\u2588")
            elif col == 0:
                line.append(" ")
            elif col == 2:
                line.append("\u2218")
        print("".join(line))

def example():
    
    tinker_toy = Figlet(font='tinker-toy')
    nancyj_fancy = Figlet(font='nancyj-fancy')

    print(tinker_toy.renderText('A - S T A R'))
    print(nancyj_fancy.renderText('algorithm'))

    finish = False

    print("""
--------------------------------------------------------------------
|      Digite o número correspondente a sua respectiva opção:      |
|                                                                  |
|  AUTO(1) -> Labirinto pré carregado no programa, apenas execute. |
|  ALEATORIO(2) -> Gere um novo labirinto aleatório e execute-o.   |
|  MANUAL(3) -> Gere um novo labirinto aleatório e execute-o.      |
|                                                                  |
|  OBS: Caso escolha a opção 3, certifique-se de que seu arquivo   |
|  esteja na pasta raiz do arquivo de inicialização main.py.       |
--------------------------------------------------------------------
""")

    while finish is False:

        escolha = input("Digite um número: ")

        if escolha == "1":
            executeSearch("preset.csv")
            finish = True
        elif escolha == "2":
            largura = int(input("Digite um número inteiro para largura: \n"))
            altura = int(input("Digite um número inteiro para altura: \n"))
            make_maze(largura, altura)
            executeSearch("random.csv")
            finish = True
        elif escolha == "3":
            filename = input("Por favor, escreva o nome do seu arquivo, por exemplo: meu_arquivo.csv\n")
            try:
                executeSearch(filename)
            except:
                print("Arquivo não encontrado na pasta.\n")
            finish = True
        else:
            print("""
    Por favor, digite um número válido:
    AUTO(1) -> Labirinto pré carregado no programa, apenas execute.
    ALEATORIO(2) -> Gere um novo labirinto aleatório e execute-o.
    MANUAL(3) -> Gere um novo labirinto aleatório e execute-o.
    """)

example()
