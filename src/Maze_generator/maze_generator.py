from random import shuffle, randrange
import csv

def make_maze(w: int = 16, h: int = 16):
    vis = ([[0] * w + [1] for _ in range(h)] + [[1] * (w + 1)])
    # print(vis)
    hor = ([["1,1,1,"] * w + ["1"] for _ in range(h + 1)])
    # print(hor)
    ver = ([["1,0,0,"] * w + ["1"] for _ in range(h)] + [[]])
    # print(ver)

    def walk(x: int, y: int):
        vis[y][x] = 1
            #cima        direita     baixo       direita
        d = [(x - 1, y), (x, y + 1), (x + 1, y), (x, y - 1)]
        shuffle(d)
        for (xx, yy) in d:
            if vis[yy][xx]:
                continue
            if xx == x:
                hor[max(y, yy)][x] = "1,0,0,"
            if yy == y:
                ver[y][max(x, xx)] = "0,0,0,"
            walk(xx, yy)

    walk(randrange(w), randrange(h))

    s = ""
    for (a, b) in zip(hor, ver):
        s += "".join(a + ["\n"] + b + ["\n"])

    with open("random.csv", "w") as csvfile:
        for caractere in s:
            csvfile.write(caractere)

    return s

if __name__ == "__main__":
    print(make_maze())
