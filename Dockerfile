FROM python:3.9-slim

COPY requirements.txt /tmp/

RUN pip install -r /tmp/requirements.txt

RUN useradd --create-home python
WORKDIR /home/python
USER python

COPY . .

CMD [ "python", "./main.py" ]
